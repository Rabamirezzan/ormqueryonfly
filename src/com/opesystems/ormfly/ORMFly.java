package com.opesystems.ormfly;

import java.util.ArrayList;
import java.util.List;

import com.opesystems.ormfly.utils.AttributeParser;
import com.opesystems.ormfly.utils.AttributeParserFactory;
import com.opesystems.ormfly.utils.ObjectParserFactory;
import com.opesystems.ormfly.utils.PrimitiveParserFactory;
import com.opesystems.ormfly.utils.RowParser;
import com.opesystems.ormfly.utils.SQLArgsUtils;
import com.opesystems.ormfly.utils.Table;
import com.opesystems.ormfly.utils.exceptions.NoAttributeParserException;

/**
 * Simple ORM.<br/>
 * Mapping SQL Results to Java Object on fly.
 * 
 * @author rramirezb
 * 
 */
public class ORMFly {

	private boolean silentNoAttributeParserExceptions;

	public ORMFly() {
		silentNoAttributeParserExceptions = false;
	}

	public ORMFly(boolean silentNoAttributeParserExceptions) {
		this.silentNoAttributeParserExceptions = silentNoAttributeParserExceptions;
	}
	
	public void setSilentNoAttributeParserExceptions(
			boolean silentNoAttributeParserExceptions) {
		this.silentNoAttributeParserExceptions = silentNoAttributeParserExceptions;
	}
	
	public boolean isSilentNoAttributeParserExceptions() {
		return silentNoAttributeParserExceptions;
	}

	/**
	 * This methods return a list of T class instances. Attributes of T have the
	 * alternative to match column name of query with {@link ColumnNameAliases} annotation.
	 * 
	 * @param db
	 *            , a Database wrapper
	 * @param clazz
	 *            , the class of the list's elements.
	 * @param sql
	 *            , SQL query. Supports question mark placeholders
	 * @param selectionArgs
	 *            , the args that replace question mark placeholders on SQL
	 *            query.
	 * @return a list of T class instances.
	 */
	@SuppressWarnings("unchecked")
	public <T> List<T> rawQuery(ORMFlyDatabase db, Class<T> clazz, String sql,
			Object... selectionArgs) {

		String[] args = SQLArgsUtils.toString(selectionArgs);
		boolean primitive = PrimitiveParserFactory.isPrimitive(clazz);
		boolean custom = AttributeParserFactory.isCustom(clazz);
		RowParser<?> parser = null;
		if (primitive || custom) {
			parser = PrimitiveParserFactory.getParser(clazz, db.getFirstIndex());
		} else {
			parser = ObjectParserFactory.create(clazz,
					silentNoAttributeParserExceptions);
		}

		Table cursor = db.query(sql, args);

		List<T> result = new ArrayList<T>();
		while (cursor.hasNext()) {

			result.add((T) parser.parse(cursor));

			cursor.moveToNext();
		}
		cursor.close();
		return result;
	}

	/**
	 * This methods return a list of T class instances. Attributes of T have the
	 * alternative to match column name of query with {@link ColumnNameAliases} annotation.
	 * 
	 * @param db
	 *            , a Database wrapper
	 * @param clazz
	 *            , the class of the list's elements.
	 * @param sql
	 *            , SQL query. Supports question mark placeholders
	 * @param selectionArgs
	 *            , the args that replace question mark placeholders on SQL
	 *            query.
	 * @return a list of T class instances.
	 */
	@SuppressWarnings("unchecked")
	public <T> T rawQueryUnique(ORMFlyDatabase db, Class<T> clazz,
			T defaultValue, String sql, Object... selectionArgs) {

		List<T> result = rawQuery(db, clazz, sql, selectionArgs);
		T e = null;
		if (!result.isEmpty()) {
			Object o = result.get(0);
			e = (T) o;
		} else {
			e = defaultValue;
		}
		return e;
	}

	public <A> void registerAttributeParser(Class<A> clazz,
			Class<? extends AttributeParser<A>> parserClass) {
		AttributeParserFactory.registerParser(clazz, parserClass);
	}
}
