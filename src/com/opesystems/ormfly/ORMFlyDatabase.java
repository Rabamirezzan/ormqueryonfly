package com.opesystems.ormfly;

import com.opesystems.ormfly.utils.Table;

/**
 * Database wrapper
 * @author rramirezb
 *
 */
public interface ORMFlyDatabase {
	public int getFirstIndex();
	public Table query(String sql, String... args);
}
