package com.opesystems.ormfly.utils;


public class ObjectParserFactory {
	

	public static <T> RowParser<T> create(final Class<T> clazz,
			final boolean silentNoAttributeParserException) {
		ObjectRowParser<T> parser = new ObjectRowParser<T>(clazz);
		parser.setSilentNoAttributeParserException(silentNoAttributeParserException);
		return parser;
	}
}
