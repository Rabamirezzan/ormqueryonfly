package com.opesystems.ormfly.utils;

public interface AttributeParser<V> {
	public V parser(String rawData);
}
