package com.opesystems.ormfly.utils;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;

import com.opesystems.ormfly.utils.exceptions.NoParseException;

public class PrimitiveParserFactory {
	private static Map<Class<?>, RowParser> instances;

	public static Map<Class<?>, RowParser> getInstances() {
		if (instances == null) {
			instances = new HashMap<Class<?>, RowParser>();
		}
		return instances;
	}

	@SuppressWarnings("unchecked")
	public static <T> RowParser<T> getParser(Class<T> clazz, int firstIndex) {
		RowParser<T> parser = null;
		if (getInstances().containsKey(clazz)) {
			parser = getInstances().get(clazz);
		} else {
			parser = create(clazz, firstIndex);
			getInstances().put(clazz, parser);
		}
		return parser;
	}

	public static boolean isPrimitive(Class<?> clazz) {
		boolean primitive = clazz.isPrimitive() || clazz.equals(byte[].class)
				|| clazz.equals(BigDecimal.class)
				|| clazz.equals(BigInteger.class)
				|| Number.class.isAssignableFrom(clazz)
				|| String.class.isAssignableFrom(clazz)
				|| Boolean.class.isAssignableFrom(clazz);

		return primitive;
	}

	private static <T> RowParser<T> create(Class<T> clazz, int firstIndex) throws NoParseException{
		RowParser<?> p = null;
		final int index = firstIndex;
		if (clazz.equals(Integer.class) || clazz.equals(Integer.TYPE)) {
			p = new RowParser<Integer>() {

				@Override
				public Integer parse(Table cursor) {

					return Integer.valueOf(cursor.getInt(index));

				}
			};
		} else if (clazz.equals(Double.class) || clazz.equals(Double.TYPE)) {
			p = new RowParser<Double>() {

				@Override
				public Double parse(Table cursor) {

					return Double.valueOf(cursor.getDouble(index));
				}
			};

		} else if (clazz.equals(Short.class) || clazz.equals(Short.TYPE)) {
			p = new RowParser<Short>() {

				@Override
				public Short parse(Table cursor) {

					return Short.valueOf(cursor.getShort(index));

				}
			};
		} else if (clazz.equals(Float.class) || clazz.equals(Float.TYPE)) {
			p = new RowParser<Float>() {

				@Override
				public Float parse(Table cursor) {

					return Float.valueOf(cursor.getFloat(index));

				}
			};

		} else if (clazz.equals(Long.class) || clazz.equals(Long.TYPE)) {
			p = new RowParser<Long>() {

				@Override
				public Long parse(Table cursor) {

					return Long.valueOf(cursor.getLong(index));

				}
			};

		} else if (clazz.equals(String.class)) {
			p = new RowParser<String>() {

				@Override
				public String parse(Table cursor) {

					return cursor.getString(index);

				}
			};

		} else if (clazz.equals(byte[].class)) {
			p = new RowParser<byte[]>() {

				@Override
				public byte[] parse(Table cursor) {

					return cursor.getBytes(index);

				}
			};

		} else if (clazz.equals(BigDecimal.class)) {
			p = new RowParser<BigDecimal>() {

				@Override
				public BigDecimal parse(Table cursor) {

					return new BigDecimal(cursor.getString(index));

				}
			};

		} else if (clazz.equals(BigInteger.class)) {
			p = new RowParser<BigInteger>() {

				@Override
				public BigInteger parse(Table cursor) {

					return new BigInteger(cursor.getString(index));

				}
			};

		} else if (clazz.equals(Boolean.class)) {
			p = new RowParser<Boolean>() {

				@Override
				public Boolean parse(Table cursor) {

					String value = cursor.getString(index);

					boolean result = false;
					if (value != null) {
						result = value.equals("1") ? true
								: value.equals("0") ? false : value
										.toLowerCase().equals("true") ? true
										: false;
					}
					return Boolean.valueOf(result);

				}
			};

		} else {
			final AttributeParser<?> attrParser = AttributeParserFactory
					.getParser(clazz);

			if (attrParser != null) {
				p = new RowParser<Object>() {

					@Override
					public Object parse(Table cursor) {
						Object value = attrParser.parser(cursor
								.getString(index));
						return value;

					}
				};
			}else{
				throw new NoParseException(clazz);
			}

		}
		return (RowParser<T>) p;
	}

	// @SuppressWarnings("unchecked")
	// private <T> T attendPrimitive(Class<T> clazz, Cursor cursor) {
	//
	// T t = null;
	// System.out.println(" " + cursor.getString(0));
	// if (cursor.getType(0) != Cursor.FIELD_TYPE_NULL) {
	// if (clazz.equals(Integer.class) || clazz.equals(Integer.TYPE)) {
	// t = (T) Integer.valueOf(cursor.getInt(0));
	// } else if (clazz.equals(Double.class) || clazz.equals(Double.TYPE)) {
	// t = (T) Double.valueOf(cursor.getDouble(0));
	// } else if (clazz.equals(Short.class) || clazz.equals(Short.TYPE)) {
	// t = (T) Short.valueOf(cursor.getShort(0));
	// } else if (clazz.equals(Float.class) || clazz.equals(Float.TYPE)) {
	// t = (T) Float.valueOf(cursor.getFloat(0));
	// } else if (clazz.equals(Long.class) || clazz.equals(Long.TYPE)) {
	// t = (T) Long.valueOf(cursor.getLong(0));
	// } else if (clazz.equals(String.class)) {
	// t = (T) cursor.getString(0);
	// } else if (clazz.equals(byte[].class)) {
	// t = (T) cursor.getBlob(0);
	// }
	// }
	// return t;
	// }
}
