package com.opesystems.ormfly.utils;

/**
 * Before first call of hasNext, the first element must be selected.
 * 
 * @author rramirezb
 *
 */

public interface Table {

	String[] getColumnNames();

	int getColumnIndex(String name);

	double getDouble(int index);

	float getFloat(int index);

	int getInt(int index);

	long getLong(int index);

	short getShort(int index);

	String getString(int index);

	byte[] getBytes(int index);

	public boolean hasNext();

	public boolean moveToNext();

	public boolean close();
}
