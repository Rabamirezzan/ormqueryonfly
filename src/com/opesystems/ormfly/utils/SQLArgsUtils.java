package com.opesystems.ormfly.utils;

public class SQLArgsUtils {
	public static String[] toString(Object... args) {
		String[] sargs = new String[args.length];
		for (int i = 0; i < args.length; i++) {
			Object arg = args[i];
			if (arg != null)
				sargs[i] = String.valueOf(arg);
		}
		return sargs;
	}
}
