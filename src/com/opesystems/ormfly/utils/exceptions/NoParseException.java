package com.opesystems.ormfly.utils.exceptions;

public class NoParseException extends RuntimeException{
	public NoParseException(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}
	String message;
	public NoParseException(Class<?> clazz) {
		init(clazz);		
	}
	
	@Override
	public String getLocalizedMessage() {
	
		return message;
	}
	
	@Override
	public String getMessage() {
	
		return message;
	}
	
	private void init(Class<?> clazz){
		message = String.format("The class %1$s has no registered an Parser<%1$s> class.", clazz.getName());
	}
}
