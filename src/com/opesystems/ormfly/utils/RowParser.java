package com.opesystems.ormfly.utils;


public interface RowParser<T> {
	public T parse(Table result);

}
