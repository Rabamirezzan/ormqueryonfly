package com.opesystems.ormfly.utils;

import java.lang.reflect.Field;
import java.util.Map;
import java.util.TreeMap;

import com.opesystems.ormfly.annotations.ColumnNameAliases;
import com.opesystems.ormfly.utils.exceptions.NoAttributeParserException;

public class ObjectRowParser<T> implements RowParser<T> {

	private Class<T> clazz;
	private boolean silentNoAttributeParserException;
	private static Map<String, Object> memory;
	
	{
		memory = new TreeMap<String, Object>();
	}

	public ObjectRowParser(Class<T> clazz) {
		super();
		this.clazz = clazz;
	}

	public boolean isSilentNoAttributeParserException() {
		return silentNoAttributeParserException;
	}

	public void setSilentNoAttributeParserException(
			boolean silentNoAttributeParserException) {
		this.silentNoAttributeParserException = silentNoAttributeParserException;
	}

	@Override
	public T parse(Table table) throws NoAttributeParserException{
		T t = null;
		try {
			t = clazz.newInstance();
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (t != null) {
			String[] names = table.getColumnNames();
			for (String name : names) {
				int index = table.getColumnIndex(name);
				Field f = getField(name, clazz, memory);
				if (f != null) {
					Object value = null;
					Class<?> fieldType = f.getType();
					if (Double.class.isAssignableFrom(fieldType)
							|| Double.TYPE.isAssignableFrom(fieldType)) {
						value = table.getDouble(index);

					} else if (Float.class.isAssignableFrom(fieldType)
							|| Float.TYPE.isAssignableFrom(fieldType)) {
						value = table.getFloat(index);
					} else if (Integer.class.isAssignableFrom(fieldType)
							|| Integer.TYPE.isAssignableFrom(fieldType)) {
						value = table.getInt(index);
					} else if (Long.class.isAssignableFrom(fieldType)
							|| Long.TYPE.isAssignableFrom(fieldType)) {
						value = table.getLong(index);
					} else if (Short.class.isAssignableFrom(fieldType)
							|| Short.TYPE.isAssignableFrom(fieldType)) {
						value = table.getShort(index);
					} else if (String.class.isAssignableFrom(fieldType)) {
						value = table.getString(index);
					} else if (byte[].class.isAssignableFrom(fieldType)
							|| Byte[].class.isAssignableFrom(fieldType)) {
						value = table.getBytes(index);
					} else {

						try {
							AttributeParser<?> attrParser = AttributeParserFactory
									.getParser(fieldType);
							value = attrParser.parser(table.getString(index));
						} catch (NoAttributeParserException e) {
							if (silentNoAttributeParserException) {
								e.printStackTrace();
							} else {
								throw e;
							}

						}
					}

					try {
						f.set(t, value);
					} catch (Exception e) {

						e.printStackTrace();
					}
				}
			}
		}
		return t;
	}

	private static enum Tool {
		NULL;
	}

	public static Field getField(String name, Class<?> clazz,
			Map<String, Object> memory) {
		Field f = null;
		if (memory.containsKey(name)) {
			Object o = memory.get(name);
			if (o instanceof Field) {
				f = (Field) o;
			}
		} else {
			Field[] fields = clazz.getDeclaredFields();
			for (Field field : fields) {
				if (field.getName().equals(name)) {
					f = field;
					break;
				} else {
					ColumnNameAliases ann = field
							.getAnnotation(ColumnNameAliases.class);
					if (ann != null) {
						String[] aliases = ann.aliases();
						for (String alias : aliases) {
							if (alias.equals(name)) {
								f = field;
								break;
							}
						}
					}
				}
			}
			if (f != null) {
				f.setAccessible(true);
				memory.put(name, f);
			} else {
				memory.put(name, Tool.NULL);
			}

		}
		return f;
	}

}
