package com.opesystems.ormfly.utils;

import java.util.HashMap;
import java.util.Map;

import com.opesystems.ormfly.utils.exceptions.NoAttributeParserException;

public class AttributeParserFactory {
	private static Map<Class<?>, AttributeParser<?>> instances;

	public static Map<Class<?>, AttributeParser<?>> getInstances() {
		if (instances == null) {
			instances = new HashMap<Class<?>, AttributeParser<? extends Object>>();
		}
		return instances;
	}

	private static <V> AttributeParser<V> create(Class<V> clazz,
			Class<? extends AttributeParser<V>> parserClass) {
		AttributeParser<V> i = null;
		try {
			i = parserClass.newInstance();
			getInstances().put(clazz, i);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return i;
	}

	public static <V> void registerParser(Class<V> clazz,
			Class<? extends AttributeParser<V>> parserClass) {
		if (!getInstances().containsKey(clazz)) {
			create(clazz, parserClass);
		}

	}

	public static <V> boolean isCustom(Class<V> clazz) {
		return getInstances().containsKey(clazz);
	}

	@SuppressWarnings("unchecked")
	public static <V> AttributeParser<V> getParser(Class<V> clazz) throws NoAttributeParserException{
		Map<Class<?>, AttributeParser<?>> is = getInstances();
		AttributeParser<V> result = null;
		if (is.containsKey(clazz)) {
			result = (AttributeParser<V>) is.get(clazz);
		} else {
			throw new NoAttributeParserException(clazz);
		}
		return result;
	}

}
