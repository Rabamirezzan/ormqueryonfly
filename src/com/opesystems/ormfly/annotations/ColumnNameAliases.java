package com.opesystems.ormfly.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Attribute aliases of a class. If the name of the attribute and the column name
 * of the {@link Table} are different, you could match the column name with an alias.
 * 
 * @author rramirezb
 * 
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface ColumnNameAliases {
	String[] aliases() default {};
}
