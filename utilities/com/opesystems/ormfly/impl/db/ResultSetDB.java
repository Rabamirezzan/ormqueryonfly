package com.opesystems.ormfly.impl.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.opesystems.ormfly.ORMFlyDatabase;
import com.opesystems.ormfly.impl.tables.ResultSetTable;
import com.opesystems.ormfly.utils.Table;
import com.opesystems.ormfly.utils.exceptions.SQLExceptionRTE;

public class ResultSetDB implements ORMFlyDatabase {

	private Connection conn;
	private ResultSetTable table = null;

	public ResultSetDB(Connection conn) {
		super();
		this.conn = conn;
		table = new ResultSetTable();
	}

	@Override
	public Table query(String sql, String... args) throws SQLExceptionRTE {
		ResultSet rs = null;
		try {
			PreparedStatement ps = conn.prepareStatement(sql, args);
			rs = ps.executeQuery();
			
		} catch (SQLException e) {

			e.printStackTrace();
			throw new SQLExceptionRTE(e);
		}
		table.setResultSet(rs);
		return table;
	}

	@Override
	public int getFirstIndex() {

		return 1;
	}

}
