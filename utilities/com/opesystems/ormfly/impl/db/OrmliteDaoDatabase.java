package com.opesystems.ormfly.impl.db;

import java.sql.SQLException;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.GenericRawResults;
import com.opesystems.ormfly.ORMFlyDatabase;
import com.opesystems.ormfly.impl.tables.GenericRawResultsTable;
import com.opesystems.ormfly.utils.Table;

public class OrmliteDaoDatabase implements ORMFlyDatabase {

	private Dao<?, ?> dao;

	public OrmliteDaoDatabase(Dao<?, ?> dao) {
		this.dao = dao;
	}

	@Override
	public int getFirstIndex() {
		
		return 0;
	}

	@Override
	public Table query(String sql, String... args) {
		GenericRawResultsTable table = null;
		try {
			GenericRawResults<String[]> results = dao.queryRaw(sql,
					args);
			table = new GenericRawResultsTable(results);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return table;
	}

}
