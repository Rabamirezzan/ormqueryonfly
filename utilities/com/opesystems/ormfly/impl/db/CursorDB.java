//package com.opesystems.ormfly.impl.db;
//
//import android.database.Cursor;
//import android.database.sqlite.SQLiteDatabase;
//
//import com.opesystems.ormfly.ORMFlyDatabase;
//import com.opesystems.ormfly.impl.tables.CursorTable;
//import com.opesystems.ormfly.utils.Table;
//import com.opesystems.ormfly.utils.exceptions.SQLExceptionRTE;
//
//public class CursorDB implements ORMFlyDatabase {
//
//	SQLiteDatabase db;
//	CursorTable table;
//
//	public CursorDB(SQLiteDatabase db) {
//		super();
//		this.db = db;
//		table = new CursorTable();
//	}
//
//	@Override
//	public Table query(String sql, String... args) throws SQLExceptionRTE {
//		Cursor cursor = null;
//
//		try {
//			cursor = db.rawQuery(sql, args);
//		} catch (Exception e) {
//
//			e.printStackTrace();
//			throw new SQLExceptionRTE(e);
//		}
//
//		table.setCursor(cursor);
//		return table;
//	}
//
//	@Override
//	public int getFirstIndex() {
//
//		return 0;
//	}
//}
