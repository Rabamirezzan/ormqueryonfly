package com.opesystems.ormfly.impl.tables;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import com.opesystems.ormfly.utils.Table;

public class ResultSetTable implements Table {

	private ResultSet resultSet;

	private String[] columnNames;
	private ResultSetMetaData metaData;

	public ResultSetTable(ResultSet resultSet) {
		super();
		setResultSet(resultSet);

	}
	
	public ResultSetTable() {
	
	}

	public void setResultSet(ResultSet resultSet) {
		this.resultSet = resultSet;
		init();
	}



	private void init() {
		try {
			metaData = resultSet.getMetaData();
			int count = metaData.getColumnCount();
			columnNames = new String[count];
			for (int i = 0; i < count; i++) {
				String name = metaData.getColumnName(i + 1);
				columnNames[i] = name;
			}
		} catch (SQLException e) {
		
			e.printStackTrace();
		}

	}

	@Override
	public String[] getColumnNames() {

		return columnNames;
	}

	@Override
	public int getColumnIndex(String name) {
		int index = -1;
		try {
			index = resultSet.findColumn(name);
		} catch (SQLException e) {
		
			e.printStackTrace();
		}
		return index;
	}

	@Override
	public double getDouble(int index) {
		double result = 0.0;

		try {
			result = resultSet.getDouble(index);
		} catch (SQLException e) {
		
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public float getFloat(int index) {
		float result = 0.0f;

		try {
			result = resultSet.getFloat(index);
		} catch (SQLException e) {
		
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public int getInt(int index) {
		int result = 0;

		try {
			result = resultSet.getInt(index);
		} catch (SQLException e) {
		
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public long getLong(int index) {
		long result = 0L;

		try {
			result = resultSet.getLong(index);
		} catch (SQLException e) {
		
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public short getShort(int index) {
		short result = 0;

		try {
			result = resultSet.getShort(index);
		} catch (SQLException e) {
		
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public String getString(int index) {
		String result = "";

		try {
			result = resultSet.getString(index);
		} catch (SQLException e) {
		
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public byte[] getBytes(int index) {
		byte[] result = new byte[] {};

		try {
			result = resultSet.getBytes(index);
		} catch (SQLException e) {
		
			e.printStackTrace();
		}
		return result;
	}

	private boolean hasNext = true;

	@Override
	public boolean hasNext() {
		boolean result = false;

		try {
			result = hasNext && resultSet.next();
		} catch (SQLException e) {
		
			e.printStackTrace();
		}

		return result;
	}

	@Override
	public boolean moveToNext() {

		boolean result = false;

		result = hasNext;

		return result;
	}

	@Override
	public boolean close() {
		boolean result = false;
		try {
			resultSet.close();
			result = true;
		} catch (SQLException e) {
		
			e.printStackTrace();
		}
		return result;
	}

}
