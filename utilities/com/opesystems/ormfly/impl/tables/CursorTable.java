//package com.opesystems.ormfly.impl.tables;
//
//import android.database.Cursor;
//
//import com.opesystems.ormfly.utils.Table;
//
//public class CursorTable implements Table {
//
//	Cursor cursor;
//	boolean hasNext = false;
//
//	public CursorTable(Cursor cursor) {
//		super();
//		setCursor(cursor);
//		
//	}
//	
//	public CursorTable() {
//	
//	}
//	
//	
//	public void setCursor(Cursor cursor) {
//		this.cursor = cursor;
//		hasNext = this.cursor.moveToFirst();
//	}
//
//	@Override
//	public String[] getColumnNames() {
//
//		return this.cursor.getColumnNames();
//	}
//
//	@Override
//	public int getColumnIndex(String name) {
//
//		return this.cursor.getColumnIndex(name);
//	}
//
//	@Override
//	public double getDouble(int index) {
//
//		return this.cursor.getDouble(index);
//	}
//
//	@Override
//	public float getFloat(int index) {
//		return this.cursor.getFloat(index);
//	}
//
//	@Override
//	public int getInt(int index) {
//		return this.cursor.getInt(index);
//	}
//
//	@Override
//	public long getLong(int index) {
//		return this.cursor.getLong(index);
//	}
//
//	@Override
//	public short getShort(int index) {
//		return this.cursor.getShort(index);
//	}
//
//	@Override
//	public String getString(int index) {
//		return this.cursor.getString(index);
//	}
//
//	@Override
//	public byte[] getBytes(int index) {
//
//		return this.cursor.getBlob(index);
//	}
//
//	@Override
//	public boolean hasNext() {
//
//		return hasNext;
//	}
//
//	@Override
//	public boolean moveToNext() {
//
//		return hasNext = cursor.moveToNext();
//	}
//
//	@Override
//	public boolean close() {
//
//		boolean result = false;
//		try {
//			cursor.close();
//			result = true;
//		} catch (Exception e) {
//
//			e.printStackTrace();
//		}
//		return result;
//	}
//}
