package com.opesystems.ormfly.impl.tables;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import com.j256.ormlite.dao.GenericRawResults;
import com.opesystems.ormfly.utils.Table;

public class GenericRawResultsTable implements Table {

	private GenericRawResults<String[]> results;
	private List<String> columnNames;
	private String[] row;
	private Iterator<String[]> iterator;
	boolean hasNext = false;

	public GenericRawResultsTable(GenericRawResults<String[]> results) {
		this.results = results;
		columnNames = Arrays.asList(this.results.getColumnNames());
		iterator = results.iterator();
		try {
			if (hasNext = iterator.hasNext()) {
				row = iterator.next();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean close() {
		try {
			results.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
	}

	/**
	 * Not supported.
	 */
	@Override
	public byte[] getBytes(int index) {

		return null;
	}

	@Override
	public int getColumnIndex(String name) {

		return columnNames.indexOf(name);
	}

	@Override
	public String[] getColumnNames() {

		return results.getColumnNames();
	}

	@Override
	public double getDouble(int index) {

		return Double.valueOf(row[index]);
	}

	@Override
	public float getFloat(int index) {

		return Float.valueOf(row[index]);
	}

	@Override
	public int getInt(int index) {

		return Integer.valueOf(row[index]);
	}

	@Override
	public long getLong(int index) {

		return Long.valueOf(row[index]);
	}

	@Override
	public short getShort(int index) {

		return Short.valueOf(row[index]);
	}

	@Override
	public String getString(int index) {

		return String.valueOf(row[index]);
	}

	@Override
	public boolean hasNext() {

		return hasNext;
	}

	@Override
	public boolean moveToNext() {

		if (hasNext = iterator.hasNext()) {
			row = iterator.next();
		}
		return hasNext;
	}

}
