package com.opesystems.ormfly.test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.opesystems.ormfly.ORMFly;
import com.opesystems.ormfly.ORMFlyDatabase;
import com.opesystems.ormfly.annotations.ColumnNameAliases;
import com.opesystems.ormfly.utils.AttributeParser;
import com.opesystems.ormfly.utils.Table;

public class Main {

	public static void main(String[] args) {
		queryDates();
//		queryModels();
	}

	/**
	 * Registering AttributeParsers
	 * 
	 * @return
	 */
	private static ORMFly init() {
		ORMFly ormFly = new ORMFly();

		// Registering an AttributeParser for java.util.Date class.
		ormFly.registerAttributeParser(Date.class, MyDateParser.class);

		return ormFly;
	}

	public static void queryDates() {

		ORMFly ormFly = init();

		// Creating a database instance with a Table of Dates.
		ORMFlyDatabase db = new MyDB(new MyDateTable());

		// Querying the first Date element.
		Date data = ormFly.rawQueryUnique(db, Date.class, new Date(),
				"Select * from SqlDummyQuery");

		System.out.println(data);
		db = new MyDB(new MyDateTable());
		// Querying a list of Date objects.
		List<Date> list = ormFly.rawQuery(db, Date.class,
				"Select * from SqlDummyQuery", "Arg1", 0, 4L);
		System.out.println(list);
	}

	private static void queryModels() {
		ORMFly ormFly = init();

		// Creating a database instance with a Table of Models.
		ORMFlyDatabase db = new MyDB(new MyModelTable());

		// Querying the first Model element.
		Model data = ormFly.rawQueryUnique(db, Model.class, null,
				"Select * from SqlDummyQuery");

		System.out.println(data);
		db = new MyDB(new MyModelTable());
		// Querying a list of Model objects.
		List<Model> list = ormFly.rawQuery(db, Model.class,
				"Select * from SqlDummyQuery", "Arg1", 0, 4L);
		System.out.println(list);
	}

	
	public static class Model {
		@ColumnNameAliases(aliases = "hello")
		String hola;

		@ColumnNameAliases(aliases = "world")
		String mundo;
		Date date;

		@Override
		public String toString() {
			return String.format("%s %s %s", hola, mundo, date);
		}
	}

	private static class MyDB implements ORMFlyDatabase {

		private Table table;

		public MyDB(Table table) {
			this.table = table;
		}

		@Override
		public Table query(String sql, String... args) {

			return table;
		}
		
		@Override
		public int getFirstIndex() {
		
			return 0;
		}

	}

	public static class MyDateParser implements AttributeParser<Date> {

		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

		@Override
		public Date parser(String rawData) {
			Date d = null;
			try {
				d = format.parse(rawData);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return d;
		}

	}

	/**
	 * This table simulates a table with 3 columns and 2 rows.
	 * @author rramirezb
	 *
	 */
	public static class MyModelTable implements Table {

		List<Map<String, String>> rows;
		int index = 0;
		String[] columnNames = new String[] { "date", "hello", "world",  };

		{
			rows = new ArrayList<Map<String, String>>();

			Map<String, String> row = new TreeMap<String, String>();
			row.put(columnNames[2], "An Example of ");
			row.put(columnNames[1], "ORM Fly");
			row.put(columnNames[0], "2013-01-01");

			rows.add(row);

			row = new TreeMap<String, String>();
			row.put(columnNames[2], "This is an example with");
			row.put(columnNames[1], "Two objects");
			row.put(columnNames[0], "2014-12-01");

			rows.add(row);
		}

		private String getValue(int index) {
			Map<String, String> row = rows.get(this.index);
			String value = row.get(columnNames[index]);
			return value;
		}

		@Override
		public String[] getColumnNames() {

			return columnNames;
		}

		@Override
		public int getColumnIndex(String name) {

			return Arrays.asList(columnNames).indexOf(name);
		}

		@Override
		public double getDouble(int index) {
			return Double.valueOf(getValue(index));
		}

		@Override
		public float getFloat(int index) {
			return Float.valueOf(getValue(index));
		}

		@Override
		public int getInt(int index) {
			return Integer.valueOf(getValue(index));
		}

		@Override
		public long getLong(int index) {

			return Long.valueOf(getValue(index));
		}

		@Override
		public short getShort(int index) {

			return Short.valueOf(getValue(index));
		}

		@Override
		public String getString(int index) {

			return getValue(index);
		}

		@Override
		public byte[] getBytes(int index) {

			return null;
		}

		@Override
		public boolean hasNext() {

			return index < rows.size();
		}

		@Override
		public boolean moveToNext() {

			return (++index) < rows.size();
		}

		@Override
		public boolean close() {

			return false;
		}

	}

	/**
	 * This table simulates a Table with one column (index 0) and 3 rows
	 * @author rramirezb
	 *
	 */
	public static class MyDateTable implements Table {
		List<String> dates;
		int i = 0;
		{
			dates = new ArrayList<String>();

			dates.add("dd2014-01-01");
			dates.add("2014-02-01");
			dates.add("2014-03-01");

		}

		@Override
		public boolean moveToNext() {

			return ++i < dates.size();
		}

		@Override
		public boolean hasNext() {

			return i < dates.size();
		}

		@Override
		public String getString(int index) {

			return dates.get(i);
		}

		@Override
		public short getShort(int index) {

			return 0;
		}

		@Override
		public long getLong(int index) {

			return 0;
		}

		@Override
		public int getInt(int index) {

			return 0;
		}

		@Override
		public float getFloat(int index) {

			return 0;
		}

		@Override
		public double getDouble(int index) {

			return 0;
		}

		@Override
		public String[] getColumnNames() {

			return null;
		}

		@Override
		public int getColumnIndex(String name) {

			return i;
		}

		@Override
		public byte[] getBytes(int index) {

			return null;
		}

		@Override
		public boolean close() {

			return false;
		}
	}

}
